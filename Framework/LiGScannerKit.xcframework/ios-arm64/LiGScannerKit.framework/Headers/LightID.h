#ifndef LightID_h
#define LightID_h

#import <simd/simd.h>

@interface LightID: NSObject<NSSecureCoding>

typedef NS_ENUM(NSUInteger, LightIDStatus) {
    LightIDStatusReady                          = 0,
    LightIDStatusNotDetected                    = 1,
    LightIDStatusNotDecoded                     = 2,
    LightIDStatusInvalidPosition                = 3,
    LightIDStatusNotRegistered                  = 4,
    LightIDStatusInvalidPositionTooClose        = 5,
    LightIDStatusDistanceRangeRestrictionNear   = 6,
    LightIDStatusDistanceRangeRestrictionFar    = 7,
    LightIDStatusInvalidPositionUnknown         = 10
};

@property (readonly, nonatomic) LightIDStatus status;
@property (readonly, nonatomic) float coordinateX;
@property (readonly, nonatomic) float coordinateY;

@property (readonly, nonatomic) BOOL isDetected;
@property (readonly, nonatomic) long deviceId;
@property (readonly, nonatomic) float detectionTime;
@property (readonly, nonatomic) float decodedTime;

@property (readonly, nonatomic) BOOL isReady;
@property (readonly, nonatomic) vector_float3 rotation;
@property (readonly, nonatomic) vector_float3 translation;
@property (readonly, nonatomic) vector_float3 position;

@property (readonly, nonatomic) matrix_float4x4 transform;

- (instancetype)initWithStatus:(LightIDStatus)status
                   coordinateX:(float)coordinateX
                   coordinateY:(float)coordinateY
                    isDetected:(BOOL)isDetected
                      deviceId:(long)deviceId
                 detectionTime:(float)detectionTime
                   decodedTime:(float)decodedTime
                       isReady:(BOOL)isReady
                      rotation:(vector_float3)rotation
                   translation:(vector_float3)translation
                      position:(vector_float3)position
                     transform:(matrix_float4x4)transform;
@end

#endif /* LightID_h */
