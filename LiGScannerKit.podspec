
Pod::Spec.new do |spec|

  spec.name         = "LiGScannerKit"
  spec.version      = "4.0.1"
  spec.summary      = "LiGScannerKit"
  spec.description  = "SDK for light tag device"
  spec.homepage     = "https://gitlab.com/lig-corp/ios-scanner-sdk-sample"
  spec.license      = "Commercial"
  spec.author             = { "Steven Lin" => "steven.lin@lig.com.tw", "Plain Wu":"plain.wu@lig.com.tw" }
  spec.platform     = :ios, "13.0"
  spec.source       = { :git => "https://gitlab.com/lig-corp/ios-scanner-sdk-sample.git", :branch => "master", :tag => "#{spec.version.to_s}"}
  spec.ios.deployment_target  = '13.0'
  spec.requires_arc = true
  spec.vendored_frameworks = "Framework/LiGScannerKit.xcframework"
end
