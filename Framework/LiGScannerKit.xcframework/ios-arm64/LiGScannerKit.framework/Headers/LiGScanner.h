#ifndef LiGScanner_h
#define LiGScanner_h

#import <UIKit/UIKit.h>
#import <ARKit/ARKit.h>
#import "LiGScannerDelegate.h"

@interface LiGScanner : NSObject

@property (readonly, atomic) BOOL isInitialized;
@property (readonly, atomic) BOOL isRunning;
@property (readonly, atomic) BOOL isDeviceSupported;
@property (readonly, nonatomic) NSString * _Nonnull version;
@property (readonly, nonatomic) NSString * _Nonnull uuid;
@property (readonly, nonatomic) NSString * _Nonnull accessToken;
@property (atomic, weak) id<LiGScannerDelegate> _Nullable delegate;

+ (instancetype _Nonnull)sharedInstance;

- (void)initialize:(NSString *_Nonnull)productKey;

- (void)deinitialize;

- (void)start;

/*!
 Stop the scanner.
 When the scanner is stopped, the delegate will receive a callback with status ScannerStatusStopped. After that, the camera is released, so that you may ENTER AR Scene.
 */
- (void)stop;

- (void)calibration:(ARCamera *_Nullable)camera;

@end

#endif /* LiGScanner_h */
