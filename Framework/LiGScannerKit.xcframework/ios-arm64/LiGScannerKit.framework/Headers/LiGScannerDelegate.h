#ifndef LiGScannerDelegate_h
#define LiGScannerDelegate_h

#import <UIKit/UIKit.h>
#import "LightID.h"

@protocol LiGScannerDelegate

typedef NS_ENUM(NSInteger, ScannerStatus) {
    ScannerStatusCorrect,
    ScannerStatusNoCameraPermission         = 121,
    ScannerStatusNoNetworkPermission        = 122,
    ScannerStatusDeviceNotSupported         = 123,
    ScannerStatusConfigFileError            = 124,
    ScannerStatusCameraRunningError         = 125,
    ScannerStatusDeviceIsSupported          = 126,

    ScannerStatusAuthenticationOk           = 300,
    ScannerStatusAuthenticationFailed       = 301,
    ScannerStatusAuthenticationTimeout      = 302,
    ScannerStatusAuthenticationInterrupted  = 303,

    // iOS only
    ScannerStatusScannerStopped             = 400
};

- (void)scannerStatus:(ScannerStatus)status;

- (void)scannerResult:(NSArray<LightID *> * _Nonnull)ids;

@end

#endif /* LiGScannerDelegate_h */
