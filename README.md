# Light Generation iOS Scanner SDK

Provides a Light Tag ID and positioning information that can be obtained from the scanner SDK.  

## Requirements
- Requires Xcode 12.5 or above.
- Use Swift Package Manager to add package dependency.
- Set the `Enable Bitcode` is `No` in Build Settings .
- Set the `Privacy - Camera Usage Description` permission.

## Authentication
Your mobile application information must be registered in our service to be successfully verified.

[Go to the website](https://cloud.lig.com.tw)
